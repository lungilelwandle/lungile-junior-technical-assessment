class Bidvest:

    def __init__(self, file_path):  
        import pandas as pd
        # just read the file here
        self.file = file_path
        df = pd.read_csv('junior_script.log', sep='\[|\]', names=['datetime', 'Log Level', 'log'], engine='python',
                         header=None)
        datetime_temp = df['datetime'].str.split(' ', n=1, expand=True)
        log_temp = df['log'].str.split(':', n=1, expand=True)
        df['Date'] = datetime_temp[0]
        df['Hour of day'] = datetime_temp[1].str.split(':', n=1, expand=True)[0]
        df['App Name'] = log_temp[0].str.replace('apps.', '')
        df['Error'] = log_temp[1]
        df['Count'] = 1
        df = df.drop(columns=['datetime', 'log'])
        stacked_clean_json = df.groupby(['Date', 'Hour of day', 'App Name', 'Log Level', 'Error'])['Count'].sum()
        entry = []
        data = []
        for index, row in stacked_clean_json.iteritems():
            entry.append(str(index).replace(')', ', \'' + str(row) + '\'').replace('(', ''))
        for plain in entry:
            data.append(plain.replace("'", "").split(','))
        self.result = data

    def json(self, custom_location="/home/bidvest-user/legacy-system-logs/bidvest.json"):
        # this will take the file out as json
        import json
        import time
        import progressbar
        output_data_json = []
        temp = dict()

        bar = progressbar.ProgressBar(redirect_stdout=True, redirect_stderr=True,
                              widgets=[' [', progressbar.Timer(), '] ', progressbar.Bar(),])

        for reco in bar(self.result):
            if (reco[0] in temp):
                if (reco[1] in temp[reco[0]]):
                    temp[reco[0]][reco[1]].append(
                        {"app_name": reco[2], "log_level": reco[3], "error": reco[4], "count": reco[5]})
                else:
                    temp[reco[0]].update({reco[1]: []})
            else:
                temp[reco[0]] = {reco[1]: []}
        output_data_json.append(temp)
        with open(custom_location, 'wb') as outputFileJson:
            json.dump(output_data_json, outputFileJson)
        outputFileJson.close()
        # so according to clients' wording here is the Dict structure in the order required. the json however will order
        # the the 3rd level in another order depend on OS
        return output_data_json

    def csv(self, custom_location="/home/bidvest-user/legacy-system-logs/bidvest.csv"):  
        # this will take the file out as csv
        import csv
        output_data_csv = self.result
        with open(custom_location, 'wb') as outputFileCsv:
            writer = csv.writer(outputFileCsv)
            writer.writerow(['Date', 'Hour of day', 'App Name', 'Log Level', 'Error', 'Count'])
            writer.writerows(output_data_csv)
        outputFileCsv.close()
        return 0
