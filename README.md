# Lungile Junior technical assessment

This is a junior technical assessment.

## Introduction

Technical AssessmentYou have been tasked to create a module that will convert a legacy system’s logs in a client’senvironment to a usable CSV formatted file with additional pre calculated values.

### Requirements

*  Must integrate with an existing python3.6 application
*  Must be deployable in a unix environment
*  Module must except a log file location
*  Module must return the location of the generated csv file
*  The module must count the amount of errors per log level, per app name, per hour ofday, per date
*  The modules must also be able to return the result in either CSV or JSON formatdepending on the users requirements

## Usage

There are many ways to use Lungile Junior, you can see a few basic examples here.

### IDE

import the class into your main code 

#### Python 2.7  

~~~~ 
from lungile_Bidvest import Bidvest
newlog = Bidvest("{{ location of log }}")
newlog.json("{{ destination location of newjson }}.json")
newlog.csv("{{ ddestination location of newcsv }}.csv")
~~~~



#### Python 3

~~~~
from .lungile_Bidvest import Bidvest
newlog = Bidvest("{{ location of log }}")
newlog.json("{{ destination location of newjson }}.json")
newlog.csv("{{ ddestination location of newcsv }}.csv")
~~~~

## Conclusion

This is easy problem although is the logs were larger in size, consider other methods like deploying an [elasticstack stack](www.elastic.co).
in an elasticstack case Deploy filebeats on the servers with log to remote send the logs to the logstash server.
deploy a pattern file to clean filebeat payloads and format them to clients needs. Then pipeline the data with workers to elasticsearch.
once at elasticsearch archieve the indices by turning them off by doing this the overall perform of elasticsearch queries has a low latency.

Another solution would be to map reduce the logs with [Apache Spark](spark.apache.org) spark in this case would work better than hadoop beacuse it would be cheaper to deploy resource once but if you alreay have a hadoop evirnoment you could use it and archive the logs too.

there are many way to solve this problem those are just a few, depends on on clients overall requirments.
